import axios from 'axios';
import {AsyncStorage} from 'react-native';
 

export function getFacebookImages() {
   // store.getState().token ;
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const token = await AsyncStorage.getItem('token');
      const items = [];
      const FacebookPhotos = await axios.get(`https://graph.facebook.com/v3.2/me?fields=albums%7Bphotos%7Bsource%7D%7D&access_token=${token}`);
      const MyImages = await axios.get('https://api.mlab.com/api/1/databases/myzesty/collections/images?apiKey=Mmlog-QrZ2fpS02UCUSx4TfdhOnm-kYj');

      FacebookPhotos.data.albums.data.map((key) => {
        items.push(...(key.photos.data));
      });

      items.map((key, index) => {
        key.id = index;
        key.src = key.source;
      });

      items.map((facebook, index) => {
        const obj2 = MyImages.data.find(obj => obj.source == facebook.source);
        if (obj2) { facebook.saved = 1; } else { facebook.saved = 0; }
      });

 
      return resolve(dispatch({
        type: 'GET_FACEBOOK_IMAGES',
        items
      }));
    } catch (error) {
      // Error retrieving data
    }
  });
}


export function ImportImages(images) {
  return dispatch => new Promise(async (resolve, reject) => {


    try {
      const SelectedImages = [...images];
      axios({ url: 'https://api.mlab.com/api/1/databases/myzesty/collections/images?apiKey=Mmlog-QrZ2fpS02UCUSx4TfdhOnm-kYj', method: 'post', data:SelectedImages })
        .then((response) => {
          return resolve(dispatch({
            type: 'IMPORT_FACEBOOK_IMAGES',
            loading: false,
          }));
        })
        .catch((error) => {
          console.log('error');
        });

    } catch (error) {
      // Error retrieving data
    }
  });
}
