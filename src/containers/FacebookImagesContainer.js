import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getFacebookImages, ImportImages} from '../actions/FacebookActions';

class FacebookImagesContainer extends Component {
 
  constructor(props) {
    super(props);
    this.state = { test:[]};
  }

  componentDidMount() {
    this.props.getFacebookImages();
  }

  GetImportImages = (data) => {
   // const { GetImportImages } = ;
      this.props.GetImportImages(data) ;
  }

  render = () => {
    const { Layout } = this.props;

    return (
      <Layout
        items={this.props.items}
        // loading={recipes.loading}
        ImportImages={this.GetImportImages}
      />
    );
  }
}

const mapStateToProps = state => ({
  items: state.facebook.items || [],
});

const mapDispatchToProps = {
  getFacebookImages,
  GetImportImages: ImportImages
};

export default connect(mapStateToProps, mapDispatchToProps)(FacebookImagesContainer);
