import { AsyncStorage } from 'react-native';
import { Facebook } from 'expo';
import axios from 'axios';
import React from 'react';
import PropTypes from 'prop-types';
import {
  Container, Content, Text, Button, ActionSheet,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Loading from './Loading';
import Messages from './Messages';
import Header from './Header';

import { Translations } from '../../i18n';
 
class About extends React.Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.state = { data: null };
  }

  static propTypes = {
    locale: PropTypes.string.isRequired,
    error: PropTypes.string,
    loading: PropTypes.bool.isRequired,
    onChangeLocale: PropTypes.func.isRequired,
  }

  static defaultProps = {
    error: null,
  }


     logIn = async () => {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions} = await Facebook.logInWithReadPermissionsAsync('278720093019588', { permissions: ['public_profile'] });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
          // const response = await fetch(`http://127.0.0.1:3000/facebook/${token}`);
          // const res = await axios.get(`https://graph.facebook.com/v3.2/me?fields=id%2Cname&access_token=${token}`);
          await AsyncStorage.setItem('token', token);

          //  const res = await fetch(`http://127.0.0.1:3000/facebook/${token}`);
        //  const res = await axios.get(`http://127.0.0.1:3000/facebook/${token}`);

        // const data = await res.data;
          // this.setState({ data: res });
        //  alert('Logged in!', ( await res.json()).name );
        // console.log(res.json());
        
        // Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
        //  Alert.alert('Logged in!', 'aaaaa');
        // 

 


      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }






  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('token');
      if (value !== null) {
        // We have data!!
        console.log(value);
        this.setState({ data: value });

      }
    } catch (error) {
      // Error retrieving data
    }
  };


  _retrieveDataFrom = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      const resw = await axios.get('http://192.168.1.3:3000/facebook/'+token);
      alert( JSON.stringify(resw) ,  JSON.stringify(resw)  ); 
    } catch (error) {
        alert(error,   error  ) ;
  };


  }

  handleChange = (locale) => {
    const { onChangeLocale } = this.props;
    onChangeLocale(locale)
      .then(() => Actions.pop)
      .catch(e => console.log(`Error: ${e}`));
  }

  changeLocale = () => {
    // Form array of possible locales eg. ['en', 'it']
    const options = Object.keys(Translations);
    options.push('Cancel');

    ActionSheet.show(
      {
        title: 'Select language',
        cancelButtonIndex: options.length - 1,
        options,
      },
      (idx) => {
        if (idx !== options.length - 1) {
          this.handleChange(options[idx]);
        }
      },
    );
  }

  render() {
    const { loading, error, locale } = this.props;

    if (loading) return <Loading />;

    return (
      <Container>
        <Content padder>
          <Header
            title="Change language"
            content=""
          />

          {error && <Messages message={error} />}

          <Button block onPress={this.logIn}>
            <Text>
          facebook
            </Text>
          </Button>

          
          <Button block onPress={this._retrieveDataFrom}>
            <Text>
            _retrieveData
            </Text>
          </Button>
          <Text> 
          {JSON.stringify(this.state.data)}
          </Text>
 
        </Content>
      </Container>
    );
  }
}

export default About ;
