import {TouchableOpacity , View, Image} from 'react-native';
import PhotoGrid from 'react-native-image-grid';
import React from 'react';
import PropTypes from 'prop-types';
import {Container, Content, Text, Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';
 
class FacebookImages extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data:[], items: [], SelectedImages: [] ,token:null,obj2:null};
  }

  // static propTypes = {
  //   locale: PropTypes.string.isRequired,
  //   error: PropTypes.string,
  //   loading: PropTypes.bool.isRequired,
  //   onChangeLocale: PropTypes.func.isRequired,
  // }

  static defaultProps = {
    error: null,
  }


  ImportImages = () => {
    this.props.ImportImages(this.state.SelectedImages);
    Actions.MyLibrary();
  }

  AddImage = (id, source) => {
    const SelectedImages = this.state.SelectedImages.concat({id:id ,source:source});
    this.setState({SelectedImages});
  }

RemoveImage = (id , source) => {
  let SelectedImages = this.state.SelectedImages.filter((value, index, arr) => {
    return value.id != id;
});
  this.setState({ SelectedImages});

}

renderItem(item, itemSize, itemPaddingHorizontal) {
  // Check these images if they have been uploaded before
  if (item.saved==1) {
    return (
      <View key={item.id} style={{ width: itemSize, height: itemSize, paddingHorizontal: itemPaddingHorizontal }} >
        <Image resizeMode="cover" style={{ flex: 1 ,opacity: 0.2 }} source={{ uri: item.src }} />
      </View>
    );
  }


  const obj = this.state.SelectedImages.find(obj => obj.id == item.id);
  if (obj) {
    return ( 
      <TouchableOpacity onPress={() => this.RemoveImage(item.id,item.src)} key={item.id}
        style={{width: itemSize, height: itemSize, paddingHorizontal: itemPaddingHorizontal, borderColor: '#833158', borderWidth: 3 }}
      >
        <Image resizeMode="cover" style={{ flex: 1 }} source={{ uri: item.src }} />
        <Image source={require('../assets/selected.png')} style={{ width: 22, height: 22, marginTop: -22, marginLeft: 20}} />

      </TouchableOpacity>
    )
  }
  return (


    <TouchableOpacity
      onPress={() => this.AddImage(item.id,item.src)}
      key={item.id}
      style={{ width: itemSize, height: itemSize, paddingHorizontal: itemPaddingHorizontal }}
    >

      <Image
        resizeMode="cover"
        style={{ flex: 1 }}
        source={{ uri: item.src }}
      />
    </TouchableOpacity>
  );
}

render() {     
  return (
    <Container>

      <View style={{paddingVertical: 7, paddingHorizontal: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' ,backgroundColor:'#d9d9d9',borderRadius:7, alignItems: 'center',marginVertical:17,marginHorizontal:17}}>
        <Image source={require('../assets/facebook.png')} style={{ width: 22, height: 22 }} />
        <Text style={{color: '#323232', marginLeft: 7 }}>facebook</Text>
      </View>


      <Content style={{ borderColor: '#b2b2b2', borderWidth: 2, marginHorizontal: 12, borderRadius: 12, }}>


        <View style={{ flexDirection: 'row',padding: 3}}>
          <TouchableOpacity style={{marginRight: 1, padding: 7, borderTopLeftRadius:10 , flexDirection: 'row',justifyContent:'center', alignSelf:'flex-start', alignItems: 'center',flex: 1,backgroundColor:'#3c5a99' }}>
            <Text style={{color: '#fff', fontSize: 16}}>images</Text>
          </TouchableOpacity>

          <TouchableOpacity style={{marginLeft: 1,padding:7, borderTopRightRadius:10 , flexDirection: 'row',justifyContent:'center', alignSelf:'flex-start', alignItems: 'center',flex: 1,backgroundColor:'#d9d9d9' }}>
            <Text style={{color: '#4d4d4d', fontSize: 16}}>Videos</Text>
          </TouchableOpacity>
        </View>


        <PhotoGrid
          data={this.props.items}
          itemsPerRow={3}
          itemMargin={0}
          itemPaddingHorizontal={0}
          renderHeader={this.renderHeader}
          renderItem={this.renderItem.bind(this)}
        />

      </Content>
         
      <View style={{ flexDirection: 'row' , marginHorizontal:12,marginVertical:9}}>
        <TouchableOpacity style={{marginRight:2,padding:7, borderTopLeftRadius:10 ,borderBottomLeftRadius:10 , flexDirection: 'row',justifyContent:'center', alignSelf:'flex-start', alignItems: 'center',flex: 1,backgroundColor:'#d9d9d9' }}>
          <Text style={{color:'#4d4d4d',fontSize:16}}>Cancel</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{marginLeft: 2, padding: 7, borderTopRightRadius: 10, borderBottomRightRadius: 10, flexDirection: 'row', justifyContent: 'center', alignSelf:'flex-start', alignItems: 'center',flex: 1,backgroundColor:'#3c5a99' }}
          onPress={this.ImportImages.bind(this)}
        >
          <Icon type="Feather" name="download" style={{ fontSize: 18, color: '#fff', lineHeight: 20, marginRight:4 }}/>
          <Text style={{ color: '#fff', fontSize: 16 }}>Import</Text>
        </TouchableOpacity>
      </View>

    </Container>

  )
}
}

export default FacebookImages;
