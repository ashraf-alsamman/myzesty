import { AsyncStorage } from 'react-native';
import { Facebook } from 'expo';
import axios from 'axios';
import { View  , TouchableOpacity ,Image } from 'react-native';
import React from 'react';
import { Text, Icon ,Container, Content, Button} from 'native-base';
import { Actions } from 'react-native-router-flux';

// , Text, Button, ActionSheet, Picker, Row, Grid ,Tabs,Tab,TabHeading,Icon,ScrollableTab

class ImportFromOtherSites extends React.Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.state = { data: [] ,items: [],token:null };
  }

  logIn = async () => {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions} = await Facebook.logInWithReadPermissionsAsync('278720093019588', { permissions: ['public_profile'] });
      if (type === 'success') {
        await AsyncStorage.setItem('token', token);
        this.setState({ token});

        Actions.FacebookImages();

      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }

  render() {
    return (
      <Container style={styles.container}> 

        <View style={{paddingHorizontal:14, paddingVertical:7, flexDirection: 'row' , alignItems: 'center'}}>
          <TouchableOpacity style={{ width: 43, flexDirection: 'row', alignSelf: 'flex-start', alignItems: 'center' }}>
            <Icon type="AntDesign" name="leftcircle" style={{ fontSize: 35, color: '#c0bebf',marginRight:7}}/>
          </TouchableOpacity>
          <Text style={{color: '#5b5c5b',fontSize:17}}>import images/vidoes from other sites</Text>
        </View>

        <Content padder>
          <Button onPress={this.logIn} full style={{backgroundColor:'#fff',borderRadius:12,marginTop:15}}>
            <Image source={require('../assets/facebook.png')} style={{width: 25, height: 25}} />
            <Text uppercase={false} style={{ color:'#444444',fontWeight:'normal'}}>import images/videos from Facebook</Text>
          </Button>

          <Button   full style={{backgroundColor:'#fff',borderRadius:12,marginTop:5}}>
            <Image source={require('../assets/pinterest.png')} style={{width: 25, height: 25}} />
            <Text uppercase={false} style={{color:'#444444',fontWeight:'normal'}}>import images/videos from Pinterest </Text>
          </Button>
        </Content>

        <TouchableOpacity >
          <View style={{ paddingVertical:10,paddingHorizontal:30,backgroundColor:'#d9d9d9',borderRadius:12, alignItems: 'center',marginVertical:17,marginHorizontal:17}}>
            <Text style={{color:'#323232'}}>Cancel</Text>
          </View>
        </TouchableOpacity>


      </Container>

    )
  }
}

export default ImportFromOtherSites ;
