import { View ,TouchableOpacity,Image} from 'react-native';
import axios from 'axios';
import React from 'react';
import PropTypes from 'prop-types';
import {
  Container, Content, Text, Picker, Row, Grid ,Tabs,Tab,TabHeading,Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';
import PhotoGrid from 'react-native-image-grid';

import Loading from './Loading';
import Messages from './Messages';
import Header from './Header';

import styles from './styles';
import TopFooter from './TopFooter';
 

import { Translations } from '../../i18n';

class MyLibrary extends React.Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.state = { data: [] ,items: [] };
  }

  static propTypes = {
    locale: PropTypes.string.isRequired,
    error: PropTypes.string,
    loading: PropTypes.bool.isRequired,
    onChangeLocale: PropTypes.func.isRequired,
  }

  static defaultProps = {
    error: null,
  }


  componentDidMount() {
    this._retrieveData();
  }


  _retrieveData = async () => {
    try {
      const mydata = await axios.get('https://api.mlab.com/api/1/databases/myzesty/collections/images?apiKey=Mmlog-QrZ2fpS02UCUSx4TfdhOnm-kYj');
      const items = mydata.data;
     
      items.map((key, index) => {
        key.id = index;
        key.src = key.source;
      });
      console.log(`all data from server${items}`);
      this.setState({ items });
    } catch (error) {
      // Error retrieving data
    }
  }

  // eslint-disable-next-line class-methods-use-this
  renderItem(item, itemSize, itemPaddingHorizontal) {
    return (
      <TouchableOpacity style={{width: itemSize, height: itemSize, paddingHorizontal: itemPaddingHorizontal }}>
        <Image resizeMode="cover" style={{ flex: 1 }} source={{ uri: item.src }} />
      </TouchableOpacity>
    )
  }

  render() {
    const { loading, error, locale } = this.props;

    if (loading) return <Loading />;

    return (
      <Container style={styles.container}>
        {/* <Text>  {JSON.stringify(this.state.items)  } </Text> */}
        <View style={{paddingRight: 20, marginTop: 10 }}>
          <TouchableOpacity onPress={Actions.ImportFromOtherSites}>
            <View style={{ backgroundColor: '#787878', borderRadius: 9, alignSelf: 'flex-end',padding: 1,width: 40,height: 35 , justifyContent: 'center', alignItems: 'center' }}  >
              <Icon type="Feather" name="download" style={{ fontSize: 22, color: '#fff', lineHeight: 20 }} />
            </View>
          </TouchableOpacity>
        </View>

        <Content padder>

          <View style={{ justifyContent: 'center', alignItems: 'center',borderBottomColor: '#b2b2b2', borderBottomWidth: 1,marginBottom:7,padding:0 }}>
            <Picker
              note
              mode="full"
              style={{ width: 90, marginBottom: 0 }}
            >
              <Picker.Item label="Nature" value="key0" />
              <Picker.Item label="ATM Card" value="key1" />
              <Picker.Item label="Debit Card" value="key2" />
              <Picker.Item label="Credit Card" value="key3" />
              <Picker.Item label="Net Banking" value="key4" />
            </Picker>
          </View>
          <PhotoGrid
            data={this.state.items}
            itemsPerRow={3}
            itemMargin={0}
            itemPaddingHorizontal={0}
            renderHeader={this.renderHeader}
            renderItem={this.renderItem.bind(this)}
          />

        </Content>

        <TopFooter />


      </Container>
    );
  }
}

export default MyLibrary;
