import { View  , TouchableOpacity  } from 'react-native';
import React from 'react';
import { Text, Icon } from 'native-base';
 
 
class TopFooter extends React.Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.state = { data: [] ,items: [] };
  }

  

  render() {
 
        
    return (
 <View style={{paddingHorizontal:14, paddingVertical:7, flexDirection: 'row' }}>
  <TouchableOpacity style={{  width: 100, flexDirection: 'row', alignSelf:'flex-start', alignItems: 'center',flex: 1 }}>
        <Icon  type='MaterialIcons' name='cancel' style={{ fontSize: 30, color: "#c0bebf",marginRight:7}}/>
        <Text style={{color:'#5b5c5b',fontSize:19}}>Cancel</Text>
  </TouchableOpacity>

  <TouchableOpacity style={{ paddingLeft: 8, width: 100, flexDirection: 'row', alignSelf: 'flex-end', alignItems: 'center',flex: 1,justifyContent: 'flex-end' }}>
        <Text style={{color:'#7b325b',marginRight:7,fontSize:19}}>Next</Text>
        <Icon  type='AntDesign' name='rightcircle' style={{ fontSize: 30, color: "#7b325b"  }}/>
  </TouchableOpacity>
 </View>
    )
     
  }
}

export default TopFooter ;
