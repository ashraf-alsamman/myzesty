import { StyleSheet } from 'react-native';

  export default styles = StyleSheet.create({
    


    container: {
      backgroundColor:'#f1f1f1' 
    },

    
  MainTitle: {
    color:'#252525',fontSize:30,fontWeight:'500'
  },

  CloseMainTitle: {
    marginTop:40,color:'#252525',fontSize:30,fontWeight:'500'
  },

   Content : {
    flex:1, backgroundColor:'#fff',paddingHorizontal:20, borderTopLeftRadius: 20, borderTopRightRadius: 20,
  },

  Form : {
    marginTop:20,marginBottom:30
  },
  
  Label : {
    color:'#929292',marginBottom:2,fontSize:13,flexDirection:'row',flex:1
  },
  FormLabel:{
    color:'#949494',fontSize:15,marginTop:20,marginBottom:-5
  },
  FormSeparator:{
    borderBottomColor:'#dfdfdf',borderBottomWidth:1,marginTop:40, paddingBottom:15,marginLeft:20
  },
  FormSeparatorText:{
    fontSize:15,color:'#929292'
  },

  PickerInternalStyle : {
    color:'#2e2e2e',marginLeft:-8,paddingLeft:-8,marginTop:-10
  },

  PickerContainer : {
     marginTop:15,paddingLeft:0,borderBottomColor:'#dfdfdf',borderBottomWidth:1, marginBottom:12
  },

  Item : {
    marginBottom:8   
  },
  
  TitleBody : {
     paddingTop:48,paddingBottom:20,
  },

  Title : {
    fontSize:25,paddingTop:30,textAlign:'center',paddingHorizontal:40,lineHeight:20,fontWeight:'500',color:'#252525' ,fontFamily:'CircularStd-Medium' 
  },

  TitleDescription: {
   marginTop:5, fontSize:17,textAlign:'center',paddingHorizontal:20,lineHeight:25,fontWeight:'400',color:'#969696',lineHeight:20
  },

  TitleWithIcon : {
    fontSize:24,paddingTop:15,textAlign:'center',paddingHorizontal:30,lineHeight:30,fontWeight:'500',color:'#252525',fontFamily:'CircularStd-Medium' 
  },
    TitleDescriptionWithIcon: {
    fontSize:17,marginTop:5,textAlign:'center',paddingHorizontal:20,lineHeight:10,fontWeight:'400',color:'#969696',lineHeight:20
  },
  
  ListItemWithBorder: {
    borderWidth:1,borderColor:'#efefef', borderRadius:13 ,flexDirection:'row' ,marginLeft:0,marginTop:17,paddingHorizontal:30,paddingVertical:42,
    shadowOffset: {width: 1, height: 4}, shadowOpacity: 0.1, shadowRadius: 1,elevation: 1, 
  },

  ListItemWithBottomBorder: {
    borderBottomWidth:1,borderBottomColor:'#e1e1e1', borderRadius:13 ,flexDirection:'row' ,marginLeft:0,marginTop:2,paddingHorizontal:30,paddingVertical:35,
  },
 
  AddButtonSection: {
    width: '100%',justifyContent: 'center',alignItems: 'center',marginTop:30,
  },

  // Swiper start
  SwiperContainer: {
    borderTopLeftRadius: 30,borderTopRightRadius: 30,height:242, backgroundColor:"#fff", paddingHorizontal: 0,paddingTop: 17,justifyContent: 'center',
  },
 HomeSwiperContainer: {
    paddingHorizontal:15, borderTopLeftRadius: 30,borderTopRightRadius: 30,height:242, backgroundColor:"#fff", paddingHorizontal: 0,paddingTop: 17,justifyContent: 'center',
  },
  
  SwiperSlide:{
    borderRadius:20,height:180, justifyContent: 'center', backgroundColor: 'transparent',flexDirection:'row'
   
  },
  wrapper:{
    borderRadius:20,
    paddingHorizontal:15
  },
  Homewrapper:{
    borderRadius:20,
    paddingHorizontal:15
  },

  HomeImage: {
    borderRadius:20,flex: 1 ,paddingHorizontal:0,marginHorizontal:15
  },
  image: {
    borderRadius:20,flex: 1 ,paddingHorizontal:15
  },
  // Swiper end

// homeactivity start

preTitle: {
  textAlignVertical: "center",textAlign: "center",color:'#d8d8d8',fontSize:17,marginTop:30
},


ButtonAdd:{
  textAlign: "center",marginTop:20,
}
,
AddButtonSection: {
  width: '100%',justifyContent: 'center',alignItems: 'center',marginTop:30,
}
// homeactivity end
,
HomeCol:{
  backgroundColor:'#fff',borderTopColor:'#e9e9e9',borderTopWidth:1,paddingTop:15,marginTop:10
},

tabStyle:{backgroundColor: 'transparent',borderRightWidth:1,borderColor:'#fff',borderTopLeftRadius: 20, borderTopRightRadius: 20,
 
},
textStyle:{backgroundColor: 'transparent',borderTopLeftRadius: 20, borderTopRightRadius: 20,},
activeTabStyle:{backgroundColor: 'transparent',borderTopLeftRadius: 20, borderTopRightRadius: 20,},
activeTextStyle:{backgroundColor: 'transparent',borderTopLeftRadius: 20, borderTopRightRadius: 20,},
TabHeading:{ borderRightWidth:1,borderColor:'#5b5f5e',flexDirection:'column',justifyContent:'center',alignItems:'center',padding:0,height:35},
TabHeadingIcon:{fontSize:15},
TabHeadingText:{fontSize:15},


ChangeLanguageRow:{
  flex:1,width:400, fontSize:17,backgroundColor:'#fff',alignItems:'center',alignContent:'center',
paddingVertical:30,marginVertical:30 
},

itemDivider:{
  color:'#9B9B9B',paddingLeft:7,fontSize:15
}


});

 
