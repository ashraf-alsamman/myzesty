import Store from '../store/facebook';

export const initialState = Store;

export default function facebookReducer(state = initialState, action) {
  switch (action.type) {
    case 'GET_FACEBOOK_IMAGES': {
      return {
        ...state,
        items: action.items,
      };
    }
    case 'IMPORT_FACEBOOK_IMAGES': {
      return {
        ...state,
        loading: action.loading,
      };
    }


    default:
      return state;
  }
}
